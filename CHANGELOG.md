# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/KasiaSmolarska/test-project/compare/v0.1.1...v0.1.2) (2021-01-16)


### Bug Fixes

* **Yml:** Adding standard version + ci pipeline ([c0a2a30](https://gitlab.com/KasiaSmolarska/test-project/commit/c0a2a3037cbba82acbd28be4de97e50e86beb0a0))

### 0.1.1 (2021-01-16)


### Bug Fixes

* Adding standard version + ci pipeline ([cd563e5](https://gitlab.com/KasiaSmolarska/test-project/commit/cd563e573eff65fe671cdc80bb0ef2d2a8081eaa))

## 0.1.0 (2021-01-16)


### Bug Fixes

* Adding standard version + ci pipeline ([cd563e5](https://gitlab.com/KasiaSmolarska/test-project/commit/cd563e573eff65fe671cdc80bb0ef2d2a8081eaa))
